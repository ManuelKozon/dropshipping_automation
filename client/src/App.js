import React, { useEffect, useState } from 'react';
import axios from 'axios';
// import './App.css'; // Optional: Import CSS for styling
import Header from './components/Header';
import MainContent from './components/MainContent';
import Footer from './components/Footer';

const App = () => {
    // Example state to hold orders fetched from API
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        // Function to fetch orders from your API (replace with actual API endpoint)
        const fetchOrders = async () => {
            try {
                const response = await axios.get('https://api.example.com/orders');
                setOrders(response.data); // Assuming response.data is an array of orders
            } catch (error) {
                console.error('Error fetching orders:', error);
            }
        };

        fetchOrders();
    }, []); // Empty dependency array ensures useEffect runs only once on component mount

    return (
        <div className="App">
            <Header />
            <MainContent orders={orders} />
            <Footer />
        </div>
    );
};

export default App;

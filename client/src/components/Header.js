// Header.js

import React from 'react';

const Header = () => {
    return (
        <header>
            <nav>
                <div className="logo">Your Logo</div>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/orders">Orders</a></li>
                    <li><a href="/settings">Settings</a></li>
                </ul>
            </nav>
        </header>
    );
};

export default Header;

import React from 'react';

const MainContent = ({ orders }) => {
    return (
        <div className="MainContent">
            <h2>Orders from API</h2>
            <ul>
                {orders.map((order, index) => (
                    <li key={index}>
                        {/* Display order details */}
                        <p>Order ID: {order.id}</p>
                        <p>Product: {order.productName}</p>
                        {/* Add more fields as per your API response */}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default MainContent;


const express = require('express');
const mongoose = require('mongoose');
const axios = require('axios');
const bodyParser = require('body-parser');
const app = express();

// Middleware to parse JSON
app.use(bodyParser.json());

// Connect to MongoDB
mongoose.connect('mongodb://localhost:27017/dropshipping');

const ProductSchema = new mongoose.Schema({
  amazonId: String,
  aliexpressId: String,
  price: Number,
  stock: Number
});

const Product = mongoose.model('Product', ProductSchema);

// Endpoint to get products
app.get('/products', async (req, res) => {
  const products = await Product.find();
  res.json(products);
});

// Endpoint to add a new product
app.post('/products', async (req, res) => {
  const { amazonId, aliexpressId, price, stock } = req.body;
  const product = new Product({ amazonId, aliexpressId, price, stock });
  await product.save();
  res.status(201).json(product);
});

// Endpoint to synchronize product information
app.post('/sync-product', async (req, res) => {
  const { amazonId, aliexpressId } = req.body;
  const amazonProduct = await getAmazonProduct(amazonId);
  const aliexpressProduct = await getAliExpressProduct(aliexpressId);

  const product = new Product({
    amazonId,
    aliexpressId,
    price: aliexpressProduct.price,
    stock: aliexpressProduct.stock
  });

  await product.save();
  res.status(201).json(product);
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
